import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import { connect } from 'react-redux';
import { WebBrowser } from 'expo';

import * as Colors from '../../constants/Colors';
import Compliencies from '../../constants/Compliencies';
import { RobotoRegularText, RobotoCondensedRegularText } from '../../components/StyledText';
import Compliency from './Compliency';

class Profils extends React.Component {
  render() {
    const { preferences } = this.props;

    return (
      <View style={styles.container}>
        <RobotoRegularText style={styles.title}>Profil</RobotoRegularText>
        <View style={styles.dividerStyle} />
        {
          preferences.allergies.map((compliency, index) => {
            if (compliency.type === Compliencies.Types.EXCLUDE)
                return null;
            const image = Compliencies.Compliencies.find(profil => compliency.id === profil.id).image;
            return (
                <React.Fragment key={index}>
                    <Compliency name={compliency.text} image={image} index={index} active={compliency.active} />
                    <View style={styles.dividerStyle} />
                </React.Fragment>
          )})
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'column',
    paddingTop: 20,
  },
  title: { 
    fontSize: 25,
    color: Colors.default.darkGrey,
    margin: 20,
  },
  dividerStyle:  {
    borderTopColor: 'black',
    borderTopWidth: 0.4, 
    opacity: 0.1,
  }
});


function mapStateToProps({
  preferences
}) {
  return {
    preferences
  }
}

export default connect(mapStateToProps)(Profils);
