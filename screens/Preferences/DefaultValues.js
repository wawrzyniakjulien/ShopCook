import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { WebBrowser } from 'expo';

import * as Colors from '../../constants/Colors';
import { RobotoRegularText, RobotoCondensedRegularText } from '../../components/StyledText';
import SimpleCard from '../../components/SimpleCard';
import { updateAdultsPreferences, updateChildrenPreferences } from '../../actions';

class DefaultValues extends React.Component {

  _updateChildren = (children) => {
    const { dispatch } = this.props;

    if (parseInt(children) > 0)
      dispatch(updateChildrenPreferences(parseInt(children))) 
  }

  _updateAdults = (adults) => {
    const { dispatch } = this.props;

    if (parseInt(adults) > 0)
      dispatch(updateAdultsPreferences(parseInt(adults)))
  }

  render() {
    const { preferences, dispatch } = this.props;
    const children = preferences.defaultValues.children;
    const adults = preferences.defaultValues.adults;

    return (
      <View style={styles.container}>
        <RobotoRegularText style={styles.title}>Valeurs par défaut</RobotoRegularText>
        <View style={styles.cardContainer}>
          <SimpleCard
            bodyText="Enfant(s)"
            headerText={children}
            input={true}
            keyboardType="numeric"
            inputChange={this._updateChildren}
            onPressLeft={() => {
              if (children > 0)
                dispatch(updateChildrenPreferences(children - 1))
            }}
            onPressRight={() => dispatch(updateChildrenPreferences(children + 1))}
          />
          <SimpleCard
            bodyText="Adulte(s)"
            headerText={adults}
            input={true}
            keyboardType="numeric"
            inputChange={this._updateAdults}
            onPressLeft={() => {
              if (adults > 0)
                dispatch(updateAdultsPreferences(adults - 1))
            }}
            onPressRight={() => dispatch(updateAdultsPreferences(adults + 1))}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    paddingBottom: 20,
    paddingTop: 20
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  title: {
    fontSize: 25,
    color: Colors.default.darkGrey,
    margin: 20,
  }
});


function mapStateToProps({
  preferences
}) {
  return {
    preferences
  }
}

export default connect(mapStateToProps)(DefaultValues);
