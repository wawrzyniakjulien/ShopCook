import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import Swiper from 'react-native-deck-swiper';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';

import { updateCurrentScreen, updateMealsSelected, fetchRecipeSelection, updateShoppingListy } from '../../../actions';

import HeaderScreen from '../../../components/HeaderScreen';
import Card from './Card';
import HeaderTitle from './HeaderTitle';
import Compliencies from '../../../constants/Compliencies';

class MealScreen extends React.Component {
  constructor(props) {
    super(props);
    this.refSwiper = React.createRef();
  }

  getCurrentDay = () => {
    const { mealsSelected, days } = this.props;
    return days[Math.floor(mealsSelected.length / 2)];
  }

  dayIsEmpty = (day) => {
    let nbEmpty = 0;
    if (day.diner.adults + day.diner.children <= 0)
      nbEmpty += 1;
    if ( day.lunch.adults + day.lunch.children <= 0)
      nbEmpty += 1;
    return nbEmpty;
  }

  fetchMealsSelection = () => {
    const { dispatch, meals, mealsSelected, days, budget, preferences } = this.props;

    if (days.length * 2 <= mealsSelected.length) {
      dispatch(updateCurrentScreen("ResumeSelectionScreen"));
    } else {
      const day = this.getCurrentDay();

      const budgetRemaining = budget.value - mealsSelected.filter(meal => !meal.empty).reduce((a, b) =>  parseFloat(a) + parseFloat(b.price), 0);
      
      let data = {
        budget: budgetRemaining,
        mandatoryTypes: preferences.allergies.filter(compliency => compliency.active && compliency.type === Compliencies.Types.MANDATORY).map(compliency => compliency.id),
        excludeTypes:   preferences.allergies.filter(compliency => compliency.active && compliency.type === Compliencies.Types.EXCLUDE).map(compliency => compliency.id),
        nbMeals: (days.length * 2) - mealsSelected.filter(meal => meal.empty).length - days.map(day => this.dayIsEmpty(day)).reduce((a, b) => a + b, 0)
      }  

      if (mealsSelected.length % 2 === 1) {
        data.mealType = "diner";
        data.nbAdults = day.diner.adults;
        data.nbChildren = day.diner.children;
        dispatch(fetchRecipeSelection(data));
      } else {
        data.mealType = "lunch";
        data.nbAdults = day.lunch.adults;
        data.nbChildren = day.lunch.children;
        dispatch(fetchRecipeSelection(data));
      }
    }
  }

  _onLike = (index) => {
    const { meals, mealsSelected,  dispatch } = this.props;
    const day = this.getCurrentDay();
    const { nbPeople, mealType, nbPeopleCoefficient } = this.getMealInfos(day);

    dispatch(updateMealsSelected([...mealsSelected, { ...meals[index], day: day.day, nbPeople, nbPeopleCoefficient, mealType }]));
  }

  emptyMeal = () => {
    const { mealsSelected,  dispatch } = this.props;

    dispatch(updateMealsSelected([...mealsSelected, {empty: true}]));
    return { nbPeople: -1, mealType: null };
  }

  getMealInfos = (day) => {
    const { mealsSelected } = this.props;

    if (day === undefined)
      return { nbPeople: 0, mealType: null };

    if (mealsSelected.length % 2 === 1) {
      if (day.diner.adults + day.diner.children <= 0)
        return this.emptyMeal();
      return { nbPeople: day.diner.adults + day.diner.children, nbPeopleCoefficient: day.diner.adults + day.diner.children * 0.5, mealType: "Soir" };
    }
    if ( day.lunch.adults + day.lunch.children <= 0)
      return this.emptyMeal(); 
    return { nbPeople: day.lunch.adults + day.lunch.children, nbPeopleCoefficient: day.lunch.adults + day.lunch.children * 0.5, mealType: "Midi" };
  }

  _swipeLeft = () => {
    this.refSwiper.current.swipeLeft();
  }

  _swipeRight = () => {
    this.refSwiper.current.swipeRight();
  }

  render() {
    const { meals, onLoad, currentMealIndex } = this.props;
    const day = this.getCurrentDay();
    const { nbPeople, mealType } = this.getMealInfos(day);
    if (nbPeople === -1)
      return null;
    if (meals.length === 0 && !onLoad)
      this.fetchMealsSelection();
    return (
      <View style={styles.container}>
        <HeaderScreen name="Meal" color="white" backgroundColor="transparent">
          {day === undefined ? null : (
            <HeaderTitle day={day.day} nbPeople={nbPeople} mealType={mealType} />
          )}
        </HeaderScreen>
        {
          (day === undefined || meals.length === 0) ? null : (
          <Animatable.View style={styles.containerSwipe} animation="fadeIn">
            <Swiper
                cards={meals}
                renderCard={(card) => (
                    <Card swipeLeft={this._swipeLeft} swipeRight={this._swipeRight} card={{...card, index: meals.findIndex((meal) => meal.id === card.id)}} />
                  )
                }
                verticalSwipe={false}
                cardIndex={currentMealIndex < meals.length ? currentMealIndex : 0}
                onSwipedRight={(index) => this._onLike(index)}
                infinite={true}
                disableTopSwipe
                disableBottomSwipe   
                cardVerticalMargin={15}
                backgroundColor={'transparent'}
                showSecondCard={false} 
                stackSize={0}
                ref={this.refSwiper}
                >
            </Swiper>
          </Animatable.View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
 containerSwipe: {
    flex: 1,
    paddingBottom: 20
  },
});


function mapStateToProps({
  budget,
  days,
  preferences,
  meals,
  mealsSelected,
  onLoad,
  recipes,
  currentMealIndex,
}) {
  return {
    budget,
    days,
    preferences,
    meals,
    mealsSelected,
    onLoad,
    recipes,
    currentMealIndex,
  }
}

export default connect(mapStateToProps)(MealScreen);
