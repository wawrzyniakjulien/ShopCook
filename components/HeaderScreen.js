import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  View
} from 'react-native';
import { connect } from 'react-redux';

import { previousScreen, updateCurrentScreen } from '../actions';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 400;
}  

class HeaderScreen extends React.Component {

  _onReturn = () => {
    const { dispatch, previous} = this.props;

    if (previous === undefined)
      dispatch(previousScreen())
    else
      dispatch(updateCurrentScreen(previous))
  }

  render() {
    const { name, dispatch, color, children, backgroundColor, colorSubtitle, fontSizeSubtitle, subtitle } = this.props;

    const fontSize = this.props.fontSize || 40;
 
    return (
      <View style={[styles.container, {backgroundColor: backgroundColor || 'transparent'}]}>
           <TouchableWithoutFeedback onPress={this._onReturn}>
             <Image
                source={require('../assets/images/ui/btn_back.png')}
                style={styles.imageReturn}
             />
           </TouchableWithoutFeedback>
            {  
             (children !== undefined) ? children :
              (
                <View style={{flexDirection: "column", width: "80%"}}>
                  <Text textAlign='left' style={[styles.headerTitle, { color, fontSize } ]}>{name}</Text>
                  {
                    subtitle !== undefined ?
                      (
                        <Text textAlign='left' style={[styles.headerSubtitle, { color: colorSubtitle, fontSize: fontSizeSubtitle } ]}>{subtitle}</Text>
                      ) : null
                  }
                </View>
              )
            }
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: 20,
    paddingLeft: 20,
    width: '100%',
    paddingBottom: 4,
  },
  imageReturn: {
    width: _deviceIsSmall() ? 60 : 80,
    height: _deviceIsSmall() ? 60 : 80,
  },
  headerTitle: {
    fontFamily: 'toolbox-wood-regular',
    fontSize: 40,
    color: 'black',
    marginTop: _deviceIsSmall() ? 5 : 15,
    marginLeft: 30,
  },
  headerSubtitle: {
    fontFamily: 'toolbox-wood-regular',
    fontSize: 40,
    color: 'black',
    marginTop: 15,
    marginLeft: 30,
  }
});


function mapStateToProps({
  currentScreen,
}) {
  return {
    currentScreen,
  }
}

export default connect(mapStateToProps)(HeaderScreen);

