

const MANDATORY =  "MANDATORY";
const EXCLUDE = "EXCLUDE";

const Allergies = [
    { id: 1, text: "Gluten", image: require('../assets/images/ui/gluten.jpg'), type: EXCLUDE},
    { id: 2, text: "Lactose", image: require('../assets/images/ui/lactose.jpg'), type: EXCLUDE},
    { id: 3, text: "Fruits à coque", image: require('../assets/images/ui/cacahuete.jpg'), type: EXCLUDE},
];

const Profils = [
    { id: 4, text: "Vegan", image: require('../assets/images/ui/vegan.jpg'), type: MANDATORY},
    { id: 5, text: "Végétarien", image: require('../assets/images/ui/vegetarien.jpg'), type: MANDATORY},
    { id: 7, text: "Sportif", image: require('../assets/images/ui/sportif.jpg'), type: MANDATORY},
    { id: 8, text: "Healthy", image: require('../assets/images/ui/healthy.jpg'), type: MANDATORY},
];

export default {
    Types: {
        MANDATORY,
        EXCLUDE
    },
    Compliencies: [
        ...Allergies,
        ...Profils,
    ]
};
