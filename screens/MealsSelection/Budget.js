import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import moment from 'moment-timezone';
import { connect } from 'react-redux';
import { MaterialIcons } from '@expo/vector-icons';

import * as Colors from '../../constants/Colors';

import { updateBudgetShoppingDay,clearMeals, updateDays, updateBudgetDate, updateBudgetValue, updateBudgetNbDays, updateCurrentScreen, updateMealsSelected } from '../../actions';
import { Days } from '../../constants/Days';
import { Screens } from '../../store';

import HeaderScreen from '../../components/HeaderScreen';
import SimpleHorizontalCard from '../../components/SimpleHorizontalCard';
import { ButtonAction } from '../../components/Button';
import SlideRight from '../../components/Animations/SlideRight';

class BudgetScreen extends React.Component {
  constructor(props) {
    super(props);
    const { dispatch, budget } = this.props;
    dispatch(updateBudgetDate(moment.tz(moment() ,"Europe/Paris")));

    if (budget.nbDays === 1) {
      const day = moment.tz(moment() ,"Europe/Paris").day() || 7;
      dispatch(updateBudgetShoppingDay(Days[day - 1])); 
    }
  }

  _addBudgetValue = () => {
    const { budget, dispatch } = this.props;

    dispatch(updateBudgetValue(budget.value + 1));
  }

  _removeBudgetValue = () => {
    const { budget, dispatch } = this.props;

    if (budget.value > 0)
      dispatch(updateBudgetValue(budget.value - 1));
  }

  _addBudgetNbDays = () => {
    const { budget, dispatch } = this.props;

    dispatch(updateBudgetNbDays(budget.nbDays + 1));
  }

  _removeBudgetNbDays = () => {
    const { budget, dispatch } = this.props;

    if (budget.nbDays > 0)
      dispatch(updateBudgetNbDays(budget.nbDays - 1));
  }

  _addBudgetShoppingDay = () => {
    const { budget, dispatch } = this.props;
    const index = Days.indexOf(budget.shoppingDay);

    if (index === Days.length - 1)
      dispatch(updateBudgetShoppingDay(Days[0]));
    else
      dispatch(updateBudgetShoppingDay(Days[index + 1]));
  }

  _removeBudgetShoppingDay = () => {
    const { budget, dispatch } = this.props;
    const index = Days.indexOf(budget.shoppingDay);

    if (index === 0)
      dispatch(updateBudgetShoppingDay(Days[Days.length - 1]));
    else
      dispatch(updateBudgetShoppingDay(Days[index - 1]));
  }

  _nextStep = () => {
    const { dispatch } = this.props;
    dispatch(clearMeals())
  }

  _onInputValueChange = (value) => {
    const { dispatch } = this.props;

    if (parseInt(value) > 0)
      dispatch(updateBudgetValue(parseInt(value)));
  }

  _onInputNbDayChange = (nbDays) => {
    const { dispatch } = this.props;

    if (parseInt(nbDays) > 0)
      dispatch(updateBudgetNbDays(parseInt(nbDays)));
  }

  render() {
    const { budget } = this.props;
 
    return (
      <SlideRight style={styles.container}>
        <HeaderScreen name="Budget repas" color="white" />
        <View style={styles.mainView}>
        <View style={styles.cardContainer}>
          <SimpleHorizontalCard
            bodyText="Mon budget repas"
            headerText={`${budget.value}`}
            input={true}
            unit="€"
            keyboardType="numeric"
            inputChange={this._onInputValueChange}
            onPressLeft={this._removeBudgetValue}
            onPressRight={this._addBudgetValue}
          />
          <SimpleHorizontalCard
            bodyText="Nombre de jour(s)"
            input={true} 
            keyboardType="numeric"
            inputChange={this._onInputNbDayChange}
            headerText={budget.nbDays}
            onPressLeft={this._removeBudgetNbDays}
            onPressRight={this._addBudgetNbDays}
          />
          <SimpleHorizontalCard
            date
            bodyText="Jour des courses"
            headerText={`${budget.shoppingDay.substring(0, 3)}.`}
            onPressLeft={this._removeBudgetShoppingDay}
            onPressRight={this._addBudgetShoppingDay}
          />
        </View>
        <ButtonAction name="Étape suivante" onAction={this._nextStep}>
          <MaterialIcons name="play-circle-outline" size={35} color="white" />
        </ButtonAction>
        </View>
      </SlideRight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  mainView: {
    backgroundColor: 'white',
    height: '100%',
  },
  cardContainer: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 15,
  },
  action: {
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20,
    height: '8%',
  },
  actionButton: {
    width: '70%',
    height: '100%',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.default.framboise,
  },
  actionFont: {
    fontSize: 20,
    color: 'white',
    paddingLeft: 8
  }
});


function mapStateToProps({
  budget,
}) {
  return {
    budget,
  }
}

export default connect(mapStateToProps)(BudgetScreen);
