import React from 'react';
import {
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';

import { ToolboxHunterswoodText, RobotoRegularText } from '../../components/StyledText';
import * as Colors from '../../constants/Colors';
import MenuItem from './MenuItem';
import { Screens } from '../../store';
import * as Animatable from 'react-native-animatable';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 400;
}

export default class MainMenuScreen extends React.Component {
  render() {
    return (
      <Animatable.View animation="fadeIn" style={styles.container}>
        <Image 
          style={styles.logoImage}
          source={require('../../assets/images/ui/logo.png')}
        />
        <RobotoRegularText style={styles.mainText}>Gérer votre budget en sachant ce que vous mangez.</RobotoRegularText>
        <MenuItem name={Screens.BudgetScreen} backgroundColor={Colors.default.framboise}>
          <ToolboxHunterswoodText style={styles.startItem}>
            DÉMARRER
          </ToolboxHunterswoodText>
        </MenuItem>
        <MenuItem name={Screens.RecipesScreen} backgroundColor="white">
          <ToolboxHunterswoodText style={styles.commonItem}>
            MA SÉLECTION
          </ToolboxHunterswoodText>
        </MenuItem>
        <MenuItem name={Screens.ShoppingListScreen} backgroundColor="white">
          <ToolboxHunterswoodText style={styles.commonItem}>
            MES COURSES
          </ToolboxHunterswoodText>
        </MenuItem>
        <MenuItem name={Screens.PreferencesScreen} backgroundColor="white">
          <ToolboxHunterswoodText style={styles.commonItem}>
            PROFIL
          </ToolboxHunterswoodText>
        </MenuItem>
      </Animatable.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  }, 
  logoImage: {
    width: '55%',
    height: '35%',
    resizeMode: 'contain',
  },
  mainText: {
    fontSize: _deviceIsSmall() ? 17 : 22,
    textAlignVertical: "center",
    textAlign: "center",
    color: 'white',
    width: '75%',
    marginBottom: 15, 
  },
  startItem: {
    fontSize: 40,
    color: 'white',
  },
  commonItem: {
    fontSize: 40,
    color: Colors.default.framboise,
  },
});
