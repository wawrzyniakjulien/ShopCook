import React from 'react';
import {
  Platform,
  TouchableNativeFeedback,
  TouchableHighlight,
  View,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import * as Colors from '../constants/Colors';
import Touchable from './Touchable';
import { updateStars } from '../actions';

class Stars extends React.Component {
  getIconName = (starIndex) => {
    const { stars, mealId } = this.props;
    const index = stars.findIndex((star) => star["id"] == mealId);
    const nbActive = (index === -1) ? 0 : stars[index].value;

    return (starIndex < nbActive) ? "ios-star" : "ios-star-outline";
  } 

  _updateStars = (starIndex) => {
    let { stars, mealId, dispatch } = this.props;
    const index = stars.findIndex(star => parseInt(star.id) == mealId);


    if (index === -1) {
      dispatch(updateStars([...stars, { id: mealId, value: starIndex + 1 }]));
    } else {
      if (starIndex + 1 < stars[index].value)
        stars[index].value = starIndex + 1;
      else if (starIndex + 1 === stars[index].value)
        stars[index].value = starIndex;
      else
        stars[index].value = starIndex + 1;
      dispatch(updateStars(stars));
    }
  } 

  render() {
    const { mealId, nbStars, stars } = this.props;

    const starsNb = new Array(nbStars).fill(1);

    return (
      <View style={styles.container}>
        {
          starsNb.map((star, index) => {
            return (
              <Touchable underlayColor="#fff" key={index} onPress={() => this._updateStars(index)} >  
                <Ionicons name={this.getIconName(index)} size={25} color={Colors.default.darkGrey} />
              </Touchable>
            )
          })
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
});

function mapStateToProps({
  stars,
}) {
  return {
    stars,
  }
}

export default connect(mapStateToProps)(Stars);



