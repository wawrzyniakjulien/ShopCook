import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import { connect } from 'react-redux';
import { WebBrowser } from 'expo';

import * as Colors from '../../constants/Colors';
import Compliencies from '../../constants/Compliencies';
import { RobotoRegularText, RobotoCondensedRegularText } from '../../components/StyledText';
import Compliency from './Compliency';

class Allergies extends React.Component {
  render() {
    const { preferences } = this.props;

    return (
      <View style={styles.container}>
        <RobotoRegularText style={styles.title}>Allergies/Intolérances</RobotoRegularText>
        <View style={styles.dviderStyle} />
        {
          preferences.allergies.map((compliency, index) => {
            if (compliency.type === Compliencies.Types.MANDATORY)
                return null;
            const image = Compliencies.Compliencies.find(allergy => compliency.id === allergy.id).image;

            return (
                <React.Fragment key={index}>
                    <Compliency name={compliency.text} image={image} index={index} active={compliency.active} />
                    <View style={styles.dviderStyle} />
                </React.Fragment>
          )})
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  title: {
    fontSize: 25,
    color: Colors.default.darkGrey,
    margin: 20,
  },
  dviderStyle: {
    borderBottomColor: 'black',
    borderBottomWidth: 0.4,
    opacity: 0.1,
  }
});


function mapStateToProps({
  preferences
}) {
  return {
    preferences
  }
}

export default connect(mapStateToProps)(Allergies);
