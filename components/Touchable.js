import React from 'react';
import {
  Platform,
  TouchableNativeFeedback,
  TouchableHighlight,
} from 'react-native';


class Touchable extends React.Component {
  render() {
    if (Platform.OS === "android") {
      return (
        <TouchableNativeFeedback {...this.props}>
          {this.props.children}
        </TouchableNativeFeedback>
      )
    }
    return (
      <TouchableHighlight {...this.props}>
        {this.props.children}
      </TouchableHighlight>
    )
  }
}


export default Touchable;

