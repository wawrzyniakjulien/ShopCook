import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { WebBrowser } from 'expo';
import * as Animatable from 'react-native-animatable';

import * as Colors from '../../constants/Colors';

import HeaderScreen from '../../components/HeaderScreen';
import DefaultValues from './DefaultValues';
import Allergies from './Allergies';
import Profils from './Profils';
import SlideTop from '../../components/Animations/SlideTop';

class PreferencesScreen extends React.Component {
  render() {
    return (
      <SlideTop style={styles.container}> 
        <HeaderScreen name="Préférences"  color="white" />
        <ScrollView style={styles.scrollView}>
          <Allergies />
          <Profils />
          <DefaultValues />
        </ScrollView>
      </SlideTop>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column', 
  },
  scrollView: {
    backgroundColor: 'white'
  }
});


function mapStateToProps({
  currentScreen,
}) {
  return {
    currentScreen,
  }
}

export default connect(mapStateToProps)(PreferencesScreen);
