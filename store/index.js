import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import logger from 'redux-logger';
import storage from 'redux-persist/lib/storage'
import { persistReducer, persistStore, createMigrate } from 'redux-persist';
import { Days } from '../constants/Days'; 
import Compliencies from '../constants/Compliencies';

export const Screens = {
  MainMenuScreen:     "MainMenuScreen",
  PreferencesScreen:  "PreferencesScreen",
  BudgetScreen:       "BudgetScreen",
  DayScreen:          "DayScreen",
  MealScreen:         "MealScreen",
  RecipesScreen:      "RecipesScreen",
  IngredientsScreen:  "IngredientsScreen",
  RecipeScreen:       "RecipeScreen",
  ShoppingListScreen: "ShoppingListScreen",
  ResumeSelectionScreen: "ResumeSelectionScreen",
  NotFoundMealsScreen: "NotFoundMealsScreen",
}
 
export const PreviousScreens = {
  PreferencesScreen:  "MainMenuScreen",
  MainMenuScreen:     "MainMenuScreen",
  BudgetScreen:       "MainMenuScreen",
  DayScreen:          "BudgetScreen",
  RecipesScreen:      "MainMenuScreen",
  IngredientsScreen:  "MainMenuScreen",
  RecipeScreen:       "RecipesScreen",
  ShoppingListScreen: "MainMenuScreen",
  MealScreen:         "BudgetScreen",
  ResumeSelectionScreen: "BudgetScreen",
  NotFoundMealsScreen: "MainMenuScreen",
}

// State initial
const initState = {
  currentScreen: "MainMenuScreen",
  stars: [], // { id: meal.id, value: 0 }
  recipes: [], // { id: recipeId, steps[...{ text: "blabla", id: 1 }], ingredients: [{ ...ingredient }] }
  shoppingList: {meals: [], list: []}, // { checked: true/false, custom: true/false, name: string, quantity: number, unit: string, image: string }
  recipeToDisplay: {
    recipeId: -1,
  },
  preferences: {
    allergies: Compliencies.Compliencies.map(compliency => ({ ...compliency, active: false })),
    defaultValues: {
      adults: 0,
      children: 0,
    },
  },
  budget: {
    value: 0,
    nbDays: 1,
    shoppingDay: Days[0],
    from: new Date(),
  },
  days: [], // { day: "LUNDI", lunch/diner: { adults: 0, children: 0 } }
  mealsSelected: [], // { day: "LUNDI", mealType: "diner/lunch", idRecipe: "id" }
  meals: [], // { ...meal },
  onLoad: false,
  currentMealIndex: 0,
};

// Reducers
function reducer(state = initState, action) {
  switch (action.type) {
    case 'UPDATE_CURRENT_SCREEN':
      return {
        ...state,
        currentScreen: action.payload.currentScreen,
      };
    case 'ADD_MEAL': {
      return {
          ...state,
          currentScreen: Screens.DayScreen,
          budget: {
            ...state.budget,
            nbDays: state.budget.nbDays + 1,
          }
        }
    };
    case 'UPDATE_SHOPPING_LIST':
      return {
        ...state,
        shoppingList: { ...action.payload.shoppingList }
      }
    case 'UPDATE_STARS':
      return {
        ...state,
        stars: [...action.payload.stars]
      }
    case 'UPDATE_ON_LOAD':
      return {
        ...state,
        onLoad: action.payload.onLoad,
      };
    case 'UPDATE_RECIPE_TO_DISPLAY':
      return {
        ...state,
        recipeToDisplay: {
          recipeId: action.payload.recipeId,
          previousScreen: action.payload.previousScreen,
        },
        currentScreen: Screens.IngredientsScreen,
        currentMealIndex: action.payload.currentMealIndex,
      }
    case 'UPDATE_FULL_RECIPE_TO_DISPLAY':
      return {
        ...state,
        recipeToDisplay: {
          recipeId: action.payload.recipeId,
          nbPeople: action.payload.nbPeople,
          nbPeopleCoefficient: action.payload.nbPeopleCoefficient,
        },
        currentScreen: Screens.RecipeScreen,
      }
    case 'UPDATE_MEALS':
      return {
        ...state,
        meals: action.payload.meals,
      };
    case 'UPDATE_MEALS_SELECTED':
      return {
        ...state,
        mealsSelected: action.payload.mealsSelected,
        shoppingList:  action.payload.mealsSelected.length === 0
        ? { meals: [], list: [...state.shoppingList.list.filter(elem => elem.custom && !elem.checked)] } : state.shoppingList,
        meals: []
      }; 
    case 'REQUEST_LOAD':
      return {
        ...state,
        onLoad: true,
      };
    case 'REQUEST_SUCCESS':
      let newState = { ...state, onLoad: false }; 
      if (action.payload.data.meals && action.payload.data.meals.length > 0) {
        const meals = action.payload.data.meals.filter(meal => state.mealsSelected.findIndex(mealS => mealS.name === meal.name ) === -1);
        newState = meals.length > 0 ? newState : { ...newState, currentScreen: Screens.NotFoundMealsScreen };
        return {
          ...newState,
          meals, 
         };
      }
      else if (action.payload.data.meals && action.payload.data.meals.length <= 0)
        return {
          ...state,
          currentScreen: Screens.NotFoundMealsScreen,
          meals: [], 
          onLoad: false,        
        };
      else if (action.payload.data.recipe)
        return {
          ...state,
          onLoad: false,
          recipes: [...state.recipes.filter(recipe => recipe.id !== action.payload.data.recipe.id), action.payload.data.recipe], 
        };
      else if (action.payload.data.recipes)
        state.recipes = [...state.recipes.filter(recipe => action.payload.data.recipes.findIndex(elem => elem.id === recipe.id) === -1)]
        return {
          ...state,
          onLoad: false,
          recipes: [...state.recipes, ...action.payload.data.recipes], 
        };
      return {
        ...state,
        onLoad: false,
        currentScreen: "MainMenuScreen",
      };
    case 'REQUEST_ERROR':
      return {
        ...state,
        currentScreen: "MainMenuScreen", 
        onLoad: false,     
      };
    case 'UPDATE_DAYS':
      return {
        ...state,
        days: action.payload.days,
        onLoad: false,
        meals: [],
        currentScreen: state.currentScreen === Screens.DayScreen ? "" + Screens.DayScreen : state.currentScreen
      }
    case 'UPDATE_BUDGET_SHOPPING_DAY':
      return {
        ...state,
        budget: {
          ...state.budget,
          shoppingDay: action.payload.shoppingDay,
        }
      };
    case 'UPDATE_BUDGET_FROM':
      return {
        ...state,
        budget: {
          ...state.budget,
          from: action.payload.from,
        }
      };
    case 'CLEAR_MEALS':
      return {
        ...state,
        currentScreen: Screens.DayScreen,
        days: [],
        mealsSelected: [],
      };
    case 'UPDATE_BUDGET_NB_DAYS':
      return {
        ...state,
        budget: {
          ...state.budget,
          nbDays: action.payload.nbDays,
        }
      };
    case 'UPDATE_BUDGET_VALUE':
      return {
        ...state,
        budget: {
          ...state.budget,
          value: action.payload.value,
        }
      };
    case 'PREVIOUS_SCREEN':
      return {
        ...state,
        currentScreen: PreviousScreens[state.currentScreen],
      };
    case 'UPDATE_PREFERENCES_ALLERGIES':
      state.preferences.allergies[action.payload.index].active = action.payload.allergy.active;
      return {
        ...state,
        preferences: { ...state.preferences }, 
        
      } 
    case 'UPDATE_PREFERENCES_ADULTS':
      return {
        ...state,
        preferences: {
          ...state.preferences,
          defaultValues: {
            ...state.preferences.defaultValues,
            adults: action.payload.adults,
          },
        },
      }
    case 'UPDATE_PREFERENCES_CHILDREN':
      return {
        ...state,
        preferences: {
          ...state.preferences,
          defaultValues: {
            ...state.preferences.defaultValues,
            children: action.payload.children,
          },
        },
      }
    default:
      return state;
  }
}

// Migrations du state qui est sauvegardé sur le téléphone en fonction de la version actuelle
const migrations = {
    2: (state) => {
      return {
        ...state,
        currentScreen: Screens.MainMenuScreen,
      }
    },
    4: (state) => {
        return {
         ...state,
         currentScreen: Screens.MainMenuScreen,
         preferences: {
          allergies: Compliencies.Compliencies.map(compliency => ({ ...compliency, active: false })),
          defaultValues: state.preferences.defaultValues,
        },
      }
    },
    5: (state) => {
      return {
          ...state,
          currentScreen: Screens.MainMenuScreen,
          preferences: {
            allergies: Compliencies.Compliencies.map(compliency => ({ ...compliency, active: false })),
            defaultValues: state.preferences.defaultValues,
        },
      }
    },
}

// Configuration du state qui est sauvegardé sur le téléphone
const persistConfig = {
  key: 'root',
  storage,
  version: 5,
  blacklist: ['currentScreen'],
  migrate: createMigrate(migrations, { debug: false })
}

// Mise en place du reducer permanent
const persistedReducer = persistReducer(persistConfig, reducer);

// Configuration de Axios pour faire des requêtes vers l'API
const client = axios.create({
//  baseURL: 'https://immense-basin-39922.herokuapp.com/api',
  baseURL: 'http://apacovhzyx.cluster021.hosting.ovh.net/API/api',
  responseType: 'json'
});

// Ajout des middlewares pour : Les call API asynschrone et pour Axios
const middleware = applyMiddleware(thunkMiddleware, axiosMiddleware(client));

// Création du store
const store = createStore(persistedReducer, middleware);

// Mise en place du store qui est sauvegardé sur le téléphone
const persistor = persistStore(store);
  
export {
  store,
  persistor,
}
