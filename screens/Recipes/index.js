import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import HeaderScreen from '../../components/HeaderScreen';
import MealSelectedCard from '../../components/MealSelectedCard';
import { ButtonAction, ButtonActionSquare } from '../../components/Button';
import Ionicons from '../../node_modules/@expo/vector-icons/Ionicons';
import { updateRecipeToDisplay, updateCurrentScreen, addMeal, updateFullRecipeToDisplay } from '../../actions';
import SlideTop from '../../components/Animations/SlideTop';
import { Screens } from '../../store';

class RecipesScreen extends Component {

    _resetSelection = () => {
        const { dispatch } = this.props;
        
        dispatch(updateCurrentScreen("BudgetScreen"));
    }

    _addMeal = () => {
        const { mealsSelected, dispatch } = this.props;
        if (mealsSelected.length === 0)
            dispatch(updateCurrentScreen("BudgetScreen"));
        else
            dispatch(addMeal());
    }

    render() {
        const { mealsSelected, dispatch } = this.props;
        const mealsSelectedNotEmpty = mealsSelected.filter(meal => !meal.empty);

        const buttonHeight = Dimensions.get("window").width / 4 - 8;
        const buttonWidth = Dimensions.get("window").width / 3 - 8;

        return (
                <SlideTop style={styles.container}>
                    <HeaderScreen  name="Résumé des menus" color="white" />
                    <ScrollView style={styles.recipeList}>
                        {mealsSelectedNotEmpty.map((meal, key) => ( 
                            <MealSelectedCard key={key} onPressImage={() => dispatch(updateFullRecipeToDisplay(meal.id, meal.nbPeople, meal.nbPeopleCoefficient))} meal={meal} />
                        ))}
                        {mealsSelectedNotEmpty.length > 1 ? (
                            <View style={{height: 100, backgroundColor: 'white'}} /> 
                        ) : null} 
                    </ScrollView>
                    <View style={styles.dviderStyle} />
                    <View style={styles.actionsContainer}>
                        <ButtonActionSquare name={"Refaire sa sélection"} width={buttonWidth}  height={buttonHeight}  onAction={this._resetSelection}  />
                        <ButtonActionSquare name={"Ajouter un repas"} width={buttonWidth}  height={buttonHeight}  onAction={this._addMeal} />
                        <ButtonActionSquare name={"Ma liste de courses"} width={buttonWidth}  height={buttonHeight} onAction={() => dispatch(updateCurrentScreen(Screens.ShoppingListScreen))} />
                    </View>
                </SlideTop> 
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
    },
    actionsContainer: {
      backgroundColor: 'white',
      height: '15%',  
      flexDirection: 'row',
      alignItems: 'center',  
      justifyContent: 'space-around', 
    },
    dviderStyle: {
        borderBottomColor: 'rgba(0, 0, 0, 0.1)',
        borderBottomWidth: 0.6,
        backgroundColor: 'white',
        paddingBottom: 8
    },
    recipeList: {
        padding: 20,
        width: '100%',
        backgroundColor: 'white',
        height: '80%',
        flexDirection: 'column',
    }
  });
  

function mapStateToProps({
    budget,
    days,
    preferences,
    meals,
    mealsSelected,
    onLoad,
  }) {
    return {
      budget,
      days,
      preferences,
      meals,
      mealsSelected,
      onLoad,
    }
  }
  
export default connect(mapStateToProps)(RecipesScreen);
  