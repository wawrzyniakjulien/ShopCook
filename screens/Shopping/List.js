import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Image } from 'react-native';
import { connect } from 'react-redux';

import HeaderScreen from '../../components/HeaderScreen';
import MealSelectedCard from '../../components/MealSelectedCard';
import { ButtonAction } from '../../components/Button';
import Ionicons from '../../node_modules/@expo/vector-icons/Ionicons';
import { updateRecipeToDisplay, updateCurrentScreen, addMeal, updateFullRecipeToDisplay, fetchRecipes, updateShoppingListy } from '../../actions';
import SlideTop from '../../components/Animations/SlideTop';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import Colors from '../../constants/Colors';
import { RobotoRegularText, RobotoBoldText } from '../../components/StyledText';
import Touchable from '../../components/Touchable';
import Dialog from "react-native-dialog";
import Images from '../../constants/Images';

class ShoppingListScreen extends Component {
    constructor(props) {
      super(props);

      this.state = {
        dialogOpen: false,
        text: "",
        quantity: ""
      }

      this.updateShoppingList();
    }

    fillShoppingList = () => {
      const { mealsSelected, recipes, dispatch } = this.props;
      let { shoppingList } = this.props;
      const mealsSelectedNotEmpty = mealsSelected.filter(meal => !meal.empty);

      mealsSelectedNotEmpty.forEach(meal => {
        const indexRecipe = recipes.findIndex(recipe => recipe.id === meal.id);
        recipes[indexRecipe].ingredients.forEach(ingredient => {
          const indexList = shoppingList.list.findIndex(item => !item.custom && item.name == ingredient.name);
          if (indexList === -1) {
            shoppingList.list.push({ checked: false, removed: false, custom: false, ...ingredient, quantity: parseFloat(ingredient.cookQuantity) * meal.nbPeopleCoefficient });
          } else {
            shoppingList.list[indexList].quantity +=  parseFloat(ingredient.cookQuantity) * meal.nbPeopleCoefficient;
          }
          shoppingList.meals.push({ ...meal });
        }); 
      });
      dispatch(updateShoppingListy(shoppingList));
    }

    updateList = () => {
      const { mealsSelected, recipes, dispatch } = this.props;
      let { shoppingList } = this.props;
      const mealsSelectedNotEmpty = mealsSelected.filter(meal => !meal.empty);

      const indexMeal = mealsSelectedNotEmpty.length - shoppingList.meals.length - 1;
      for (let i = 0; i < mealsSelectedNotEmpty.length - shoppingList.meals.length; i++) {
        const indexRecipe = recipes.findIndex(recipe => recipe.id === mealsSelectedNotEmpty[i].id);
        recipes[indexRecipe].ingredients.forEach(ingredient => {
          const indexList = shoppingList.list.findIndex(item => !item.custom && item.name == ingredient.name);
          if (indexList === -1) {
            shoppingList.list.push({ checked: false, removed: false, custom: false, ...ingredient, quantity: parseFloat(ingredient.quantity) });
          } else {
            shoppingList.list[indexList].removed = false;
            shoppingList.list[indexList].checked = false;
            shoppingList.list[indexList].quantity +=  parseFloat(ingredient.quantity) * mealsSelectedNotEmpty[i].nbPeopleCoefficient;
          }
          shoppingList.meals.push({ ...mealsSelectedNotEmpty[i] });
        }); 
      }
      if (mealsSelectedNotEmpty.length - shoppingList.meals.length === 0)
        dispatch(updateShoppingListy(shoppingList));
    }

    updateShoppingList = () => {
      const { shoppingList, mealsSelected, recipes, dispatch } = this.props;
      const recipesToFetch = [];
      const mealsSelectedNotEmpty = mealsSelected.filter(meal => !meal.empty);

      mealsSelectedNotEmpty.forEach(meal => {
        if (recipes.findIndex(recipe => meal.id == recipe.id) === -1) {
          recipesToFetch.push(meal.id);
        }
      })

      if (recipesToFetch.length > 0) {
        dispatch(fetchRecipes(recipesToFetch));
      } else {
        const shoppingListWithoutCustom = shoppingList.list.filter(item => !item.custom);
        if (shoppingListWithoutCustom.length === 0 &&  mealsSelectedNotEmpty.length > 0) {
          this.fillShoppingList();
        } else if (shoppingList.meals.length < mealsSelectedNotEmpty.length) {
          this.updateList()
        }
      }
    } 

    _updateChecked = (name) => {
      const { dispatch, shoppingList } = this.props;
      const index = shoppingList.list.findIndex(elem => name === elem.name);
      shoppingList.list[index].checked = !shoppingList.list[index].checked;
      dispatch(updateShoppingListy(shoppingList));
    } 

    _addElemInList = () => {
      const { text, quantity } = this.state;
      let { shoppingList, dispatch } = this.props;

      shoppingList.list.push({custom: true, checked: false, name: text, quantity});
      dispatch(updateShoppingListy(shoppingList));
      this.setState({dialogOpen: false, text: ""});
    }

    _deleteItem = (name) => {
      let { shoppingList, dispatch } = this.props;
      const index = shoppingList.list.findIndex(elem => elem.name === name);

      if (index !== -1) {
        shoppingList.list[index].removed = true;
        dispatch(updateShoppingListy(shoppingList));
      }
    }

    render() {
        const { dispatch, shoppingList, onLoad } = this.props;

        if (!onLoad)
           this.updateShoppingList();
        
        const listFiltered = [
          ...shoppingList.list.filter(elem => !elem.removed),
        ]
        const sizeList = listFiltered.length;

        return (
                <SlideTop style={styles.container}>   
                    <HeaderScreen  name="MA LISTE DE COURSES" color="white" fontSize={30} />
                    <ScrollView style={styles.scrollView} > 
                    { listFiltered.map((elem, index) => {
                      const imagePreview = elem.custom ? require('../../assets/images/ui/stuff.jpg') : Images.getImage(elem.image, false);
                      const quantity = elem.custom ? elem.quantity : ((Math.ceil(parseFloat(elem.quantity) * 10) / 10) || "");
                      return ( 
                        <React.Fragment key={index}>
                          {index > 0 ? <View style={styles.dviderStyle} /> : null}
                          <SwipeRow
                            disableRightSwipe
                            previewOpenValue={0.1}   
                            rightOpenValue={-75}
                            previewDuration={-1000}
                            style={{ paddingBottom: 1 }}
                          >
                          <Touchable onPress={() => this._deleteItem(elem.name)}>
                            <View style={styles.rowBack}>
                              <Image style={styles.trashImage} source={require('../../assets/images/ui/picto_poubelle.png')} />
                            </View>
                          </Touchable>
                          <Touchable onPress={() => this._updateChecked(elem.name)}>  
                            <View style={[styles.rowFront, { backgroundColor: elem.checked ? Colors.whiteGrey : 'white' }]}>
                                <View>
                                  <Image style={styles.circleImage} source={imagePreview} />
                                  {
                                    elem.checked ? (
                                      <Image
                                        source={require('../../assets/images/ui/photo_ingrédients_trouvé_3.png')}
                                        style={[styles.alimentImageValidate]}
                                      />
                                    ) : null
                                  }
                                </View>
                                <RobotoRegularText style={[styles.itemText, elem.checked ? styles.strike : null]}>
                                  <RobotoBoldText>{quantity} {elem.custom ? null : elem.unit}</RobotoBoldText> {elem.name}
                                </RobotoRegularText>
                            </View> 
                          </Touchable> 
                          </SwipeRow>
                          {index + 1 >= listFiltered.length ? <View style={styles.dviderStyle} /> : null}
                        </React.Fragment>
                    )
                  }) }
                      <View style={{height: 40}} />  
                  </ScrollView> 
                    <View style={styles.dividerStyle} />  
                    <View style={styles.actionsContainer}>
                        <ButtonAction name="Ajouter" height="80%" marginTop={0} onAction={() => this.setState({dialogOpen: true})} >
                            <Ionicons name="ios-add-circle-outline" size={35} color="white" />  
                        </ButtonAction>
                    </View> 
                    <View>
                      <Dialog.Container visible={this.state.dialogOpen}>
                        <Dialog.Title style={{paddingBottom: 10}} fontSize={25}>Ajouter un élément</Dialog.Title>    
                        <Dialog.Input style={{paddingBottom: 10}} fontSize={20} label="Nom de l'ingrédient :" onChangeText={(text) => this.setState({ text })} />
                        <Dialog.Input style={{paddingBottom: 10}} fontSize={20} label="Quantité :" onChangeText={(quantity) => this.setState({ quantity })} /> 
                        <Dialog.Button label="Annuler" onPress={() => this.setState({dialogOpen: false, text: ""})} />
                        <Dialog.Button label="Ajouter" onPress={this._addElemInList} />
                      </Dialog.Container>
                   </View>
                </SlideTop>
        );    
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column', 
      justifyContent: 'space-between'
    }, 
    scrollView: {
      backgroundColor: 'white',
      height: '60%',
    },
    dividerStyle: {
      borderBottomColor: 'rgba(0, 0, 0, 0.1)',
      borderBottomWidth: 0.6, 
      backgroundColor: 'white',
    },
    strike: {
      textDecorationLine: 'line-through',
      textDecorationStyle: 'solid',
    },
    alimentImageValidate: {
      height: 50, 
      position: 'absolute',
      width: 50,
      borderRadius: 25,
      borderWidth: 2, 
      borderColor: Colors.framboise, 
    },
    actionsContainer: {
      backgroundColor: 'white',
      height: '10%', 
      flexDirection: 'column',
      justifyContent: 'space-around', 
    },
    rowFront: {
      alignItems: 'center',
      backgroundColor: 'white',
      paddingLeft: '20%', 
      flexDirection: 'row',
      height: 60,
    },
    circle: {
      height: 40,
      width: 40,
      borderRadius: 20,
      borderWidth: 2,
      borderColor: Colors.framboise, 
      backgroundColor: Colors.darkGrey
    },
    circleImage: {
      height: 50,
      width: 50,
      borderRadius: 25,
      borderWidth: 2,
      borderColor: Colors.framboise, 
    },
    rowBack: {
      alignItems: 'center',
      backgroundColor: Colors.framboise,
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      paddingRight: 15, 
    },
    itemText: {
      paddingLeft: 20,  
    },
    trashImage: {
      height: 40,
      width: 40,
      resizeMode: 'contain'
    },
    dviderStyle: {
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      height: StyleSheet.hairlineWidth,
  }
  });
  

function mapStateToProps({
    recipes,
    mealsSelected,
    shoppingList,
    onLoad,
  }) {
    return {
      recipes,
      mealsSelected,
      shoppingList,
      onLoad,
    }
  }
  
export default connect(mapStateToProps)(ShoppingListScreen);
  