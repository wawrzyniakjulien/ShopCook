import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Easing, Animated } from 'react-native';

export default class FlipAnimation extends Component {
  static defaultProps = {
    style: {},
    flipDuration: 400,
    flipEasing: Easing.out(Easing.ease),
    flipAxis: 'y',
    perspective: 1000,
  };

  constructor(props) {
    super(props);

    const frontRotationAnimatedValue = new Animated.Value(0.5);

    const interpolationConfig = { inputRange: [0, 1], outputRange: ['0deg', '300deg'] };
    const frontRotation = frontRotationAnimatedValue.interpolate(interpolationConfig);

    this.state = {
      frontRotationAnimatedValue,
      frontRotation,
    };

    window.setImmediate(() => {
      window.requestAnimationFrame(() => {
        Animated.parallel(
          [this._animateValue(this.state.frontRotationAnimatedValue, 0, this.props.flipEasing)],
        ).start();
      });
    });
  }

  _animateValue = (animatedValue, toValue, easing) => Animated.timing(
    animatedValue,
    { toValue, duration: 700, easing },
  );

  render() {
    return (
       <Animated.View
          style={[
            styles.flippableView,
            { transform: [
              { perspective: 1000 },
              { rotateY: this.state.frontRotation },
            ] },
          ]}
        >
        { this.props.children }
      </Animated.View>
    );
  }
}

FlipAnimation.propTypes = {
  flipEasing: PropTypes.object,
  children: PropTypes.element.isRequired,
};

const styles = StyleSheet.create({
  flippableView: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backfaceVisibility: 'hidden',
  },
});
