import React from 'react';
import {
  TouchableWithoutFeedback,
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';
import * as Colors from '../constants/Colors';
import { RobotoCondensedRegularText } from './StyledText';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 400;
}

export class ButtonAction extends React.Component {
  render() {
    const { name, children, onAction } = this.props;
    const height = (this.props.height) ? this.props.height : '8%';
    const marginTop = (this.props.marginTop !== undefined) ? this.props.marginTop : 20;


    return (
        <View style={[styles.action, { height, marginTop }]}>
           <TouchableWithoutFeedback onPress={() => onAction()}>
            <View style={styles.actionButton}>
              { children }
              <RobotoCondensedRegularText style={styles.actionFont}>
                { name }
              </RobotoCondensedRegularText>
            </View> 
           </TouchableWithoutFeedback>
        </View>
        )
  }
}

export class ButtonActionSquare extends React.Component {
  render() {
    const { name, children, onAction, height, width } = this.props;
 

    return (
        <View style={[styles.actionButtonSquare, {height, width }]}>
           <TouchableWithoutFeedback onPress={() => onAction()}>
              <RobotoCondensedRegularText style={styles.actionFontSquare}>
                { name }
              </RobotoCondensedRegularText>
           </TouchableWithoutFeedback>
        </View>
        )
  }
}  
 
const styles = StyleSheet.create({
  action: {
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  actionButtonSquare: {
    borderRadius: 8,
    flexDirection: 'row',
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.default.framboise,
  },
  actionButton: {
    width: '70%',
    height: '100%',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.default.framboise,
  },
  actionFont: {
    fontSize: 20,
    color: 'white',
    paddingLeft: 8
  }, 
  actionFontSquare: {
    fontSize: _deviceIsSmall() ? 17 : 22,
    color: 'white',
    paddingLeft: 8
  }
});

