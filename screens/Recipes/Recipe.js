import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import HeaderScreen from '../../components/HeaderScreen';
import * as Colors from '../../constants/Colors';
import { RobotoBoldText, RobotoRegularText } from '../../components/StyledText';
import { fetchRecipe } from '../../actions';
import Stars from '../../components/Stars';
import Feather from '../../node_modules/@expo/vector-icons/Feather';
import FontAwesome from '../../node_modules/@expo/vector-icons/FontAwesome';
import Images from '../../constants/Images';
import SlideRight from '../../components/Animations/SlideRight';

const _deviceIsSmall = () => {
    return Dimensions.get('window').width < 400;
}

class RecipeScreen extends Component {
    constructor(props) {
        super(props);

        const { dispatch, recipeToDisplay } = props;

        dispatch(fetchRecipe(recipeToDisplay.recipeId));
    }

    convertMinsToHrsMins(mins) {
        let h = Math.floor(mins / 60);
        let m = mins % 60;
        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
        return `${h}h${m}`;
      }

    render() {
        const { recipeToDisplay, recipes } = this.props;
        const index = recipes.findIndex(recipe => recipe.id == recipeToDisplay.recipeId);
        const steps = (index === -1) ? [] : recipes[index].steps; 
        const ingredients = (index === -1) ? [] : recipes[index].ingredients; 

        const name = (index === -1 || !recipes[index].infos) ? "Loading..." : recipes[index].infos.name;
        const image = (index === -1 || !recipes[index].infos || !recipes[index].infos.image) ? undefined : recipes[index].infos.image;
        const duration = (index === -1 || !recipes[index].infos) ? "Loading..." : parseInt(recipes[index].infos.duration);

        return (
            <SlideRight style={styles.container}>
                <HeaderScreen name={`RECETTE - ${name}`}  color={'white'} fontSize={30} />
                <ScrollView style={styles.mainView} > 
                    <View style={styles.headerContainer}>
                        <View  style={styles.flexColumn}>
                            <RobotoRegularText style={styles.starsTitle}>Votre avis :</RobotoRegularText>
                            <Stars nbStars={5} mealId={recipeToDisplay.recipeId} />
                        </View>   
                        <Image style={styles.circleImage} source={Images.getImage(image, true)} />
                        <View style={styles.flexColumn}>
                            <View style={styles.duration}>
                                <Feather name="clock" size={20} color={Colors.default.darkGrey} />
                                <RobotoRegularText style={styles.durationText} >
                                    {this.convertMinsToHrsMins(duration)}
                                </RobotoRegularText>  
                            </View>  
                            <View style={styles.nbPeople}>
                                <FontAwesome name="user-o" size={20} color={Colors.default.darkGrey} />
                                <RobotoRegularText style={styles.nbPeopleText} >{recipeToDisplay.nbPeople || 0}</RobotoRegularText>
                            </View>
                        </View>
                    </View>
                    <View style={{paddingBottom: 15}}> 
                        <RobotoBoldText style={styles.ingredientTitle} >INGREDIENTS :</RobotoBoldText>
                            <View style={[styles.row, {paddingBottom: 8, justifyContent: 'flex-end'}]}>
                                <View style={{paddingRight: 30}}> 
                                    <RobotoBoldText style={styles.ingredientName} >Origine</RobotoBoldText>   
                                </View>  
                            </View>
                            {ingredients.map((ingredient, index) => (
                                <React.Fragment key={index}>
                                    <View style={styles.dviderStyle} />
                                    <View style={styles.row}>
                                        <View style={{flexDirection: 'row', paddingLeft: 20, alignItems: 'center'}}>
                                            <View style={styles.ingredientPreviewContainer}>
                                                <Image style={styles.circleImageIngredient} source={Images.getImage(ingredient.image, false)} />
                                            </View>
                                            <RobotoRegularText style={styles.ingredientName} >
                                                <RobotoBoldText style={styles.ingredientQuantity}>{((Math.ceil(parseFloat(ingredient.cookQuantity * recipeToDisplay.nbPeopleCoefficient) * 10) / 10))} {ingredient.unit}</RobotoBoldText> {ingredient.name}
                                            </RobotoRegularText>
                                        </View>
                                        <View style={{paddingRight: 25}}>
                                            <RobotoRegularText style={styles.ingredientName} >Inconnue</RobotoRegularText>   
                                        </View>
                                    </View>
                                </React.Fragment>
                            ))}
                    </View>
                    {steps.map((step, index) => (
                        <React.Fragment key={index}>
                            {index > 0 ? <View style={styles.dviderStyle} /> : null }
                            <RobotoBoldText style={styles.stepNumber} >ETAPE  {step.id} :</RobotoBoldText>
                            <View style={styles.stepContentContainer} >
                                <RobotoRegularText style={styles.stepText}>{step.text}</RobotoRegularText>
                            </View>
                        </React.Fragment>
                    ))}
                    <View style={styles.dviderStyle} />
                    <View style={styles.footerContainer}>
                        <RobotoBoldText style={styles.footerText} >BON APPETIT !</RobotoBoldText>
                    </View>
                </ScrollView>
            </SlideRight>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
    },
    mainView: {
        backgroundColor: 'white',
        height: '100%',
      },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 16,
        paddingLeft: 25,
        paddingRight: 25,
        paddingBottom: 25,
    },
    starsTitle: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        color: Colors.default.darkGrey,
        fontSize: 15
    },
    stepContentContainer: {
        flexDirection: 'row',
        padding: 20,
        justifyContent: 'space-around',  
    },
    circleImage: {
        height: 130,
        width: 130,
        marginTop: _deviceIsSmall() ? 25 : 20,
        marginRight: _deviceIsSmall() ? 0 : 20,
        borderRadius: 65,
        borderWidth: 2,
        borderColor: Colors.default.framboise,
    },
    miniCircle: {
        height: 60,
        width: 60,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: Colors.default.framboise,
        backgroundColor: Colors.default.darkGrey
    },
    flexColumn : {
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    duration: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    durationText: {
        fontSize: 15,
        paddingLeft: 4,
        color: Colors.default.darkGrey
    },
    stepText: {
        fontSize: 15,
        paddingLeft: 4,
        color: Colors.default.darkGrey,
        width: '100%',
    },
    nbPeople: {
        flexDirection: 'row',
        paddingTop: 4,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    ingredientTitle: {
        fontSize: 22,
        paddingLeft: 20,
        paddingTop: 8,
        color: Colors.default.framboise,
        paddingBottom: 15
    },
    footerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: '100%',
        paddingBottom: 20
    },
    footerText: {
        fontSize: 22,
        paddingLeft: 20,
        color: Colors.default.framboise
    },
    stepNumber: {
        fontSize: 22,
        paddingLeft: 20,
        paddingTop: 8,
        color: Colors.default.framboise
    },
    nbPeopleText: {
        fontSize: 15,
        paddingLeft: 4,
        color: Colors.default.darkGrey
    },
    dviderStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        height: StyleSheet.hairlineWidth,
    },
    ///
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 1
    },
    ingredientPreviewContainer: {
        padding: 8,
    },
    circleImageIngredient: {
        height: 40,
        width: 40,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: Colors.default.framboise,
    },
    ingredientQuantity: {
        fontSize: _deviceIsSmall() ? 12 : 15,
        color: Colors.default.darkGrey
    },
    ingredientName: {
        paddingLeft: _deviceIsSmall() ? 8 : 16,
        fontSize: _deviceIsSmall() ? 12 : 15,
        color: Colors.default.darkGrey
    },
  });
  

function mapStateToProps({ recipeToDisplay, recipes }) {
    return {
        recipeToDisplay,
        recipes,
    }
  }
  
  export default connect(mapStateToProps)(RecipeScreen);
  