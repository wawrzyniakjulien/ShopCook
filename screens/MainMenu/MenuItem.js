import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import { connect } from 'react-redux';

import { updateCurrentScreen } from '../../actions';

class MenuItem extends React.Component {

  render() {
    const { backgroundColor, color, children, name, dispatch } = this.props;

    return (
      <TouchableWithoutFeedback onPress={() => dispatch(updateCurrentScreen(name))}>
        <View style={[styles.menuItem, { backgroundColor }]}>
          {children}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  menuItem: {
    width: '70%',
    height: '10%',
    borderRadius: 8,
    marginBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

function mapStateToProps({
  currentScreen,
}) {
  return {
    currentScreen,
  }
}

export default connect(mapStateToProps)(MenuItem);
