import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import * as Colors from '../../constants/Colors';
import { RobotoCondensedRegularText, RobotoRegularText } from '../../components/StyledText';

import { updateCurrentScreen } from '../../actions';

import HeaderScreen from '../../components/HeaderScreen';
import { ButtonAction } from '../../components/Button';
import SlideTop from '../../components/Animations/SlideTop';
import { Screens } from '../../store';
import SlideRight from '../../components/Animations/SlideRight';

class NotFoundMealsScreen extends React.Component {
  render() {
    const { dispatch } = this.props;

    return (
      <SlideRight style={styles.container}>
        <HeaderScreen name={"Erreur"} color="white" />
        <View style={styles.mainView}>
          <View style={styles.textContainer}>
          <RobotoCondensedRegularText textAlign="center" style={styles.title}> 
              Aucun résultat correspondant à vos critères de budget ou/et de préférences
          </RobotoCondensedRegularText> 
          </View>
          <ButtonAction name={"Refaire ma sélection"} onAction={() => dispatch(updateCurrentScreen(Screens.BudgetScreen))}>
              <Ionicons name="ios-add-circle-outline" size={35} color="white" />  
          </ButtonAction>
        </View>
      </SlideRight>
    ); 
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  mainView: {
    backgroundColor: 'white',
    height: '100%',
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    textAlign: 'center'
  }, 
  title: {
    fontSize: 25,
    textAlign: 'center',
    color: Colors.default.darkGrey,
    marginTop: 30,
    marginBottom: 30,
    width: '85%',  
  },
});


function mapStateToProps() {
  return {
  }
}

export default connect(mapStateToProps)(NotFoundMealsScreen);
