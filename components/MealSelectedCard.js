import React, { Component } from 'react';
import { View, Dimensions, StyleSheet, Image } from 'react-native';

import * as Colors from '../constants/Colors';
import { RobotoRegularText, ToolboxHunterswoodText, RobotoBoldText } from './StyledText';
import Feather from '@expo/vector-icons/Feather';
import FontAwesome from '../node_modules/@expo/vector-icons/FontAwesome';
import Touchable from './Touchable';
import Images from '../constants/Images';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 400;
}

class MealSelectedCard extends Component { 

    render() {
        const { meal, onPressImage } = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardBodyContainer}>
                  <View style={styles.cardBodyTitleContainer}>
                    <View style={styles.flexRow}>
                      <RobotoRegularText style={styles.mealType}>{meal.mealType}</RobotoRegularText>
                      <FontAwesome style={styles.user} name="user-o" size={15} color={Colors.default.darkGrey} />
                      <RobotoRegularText style={styles.userText}>{meal.nbPeople}</RobotoRegularText>
                    </View>
                    <ToolboxHunterswoodText style={styles.day}>{meal.day}</ToolboxHunterswoodText>                    
                  </View>
                  <View style={styles.cardBodyNameContainer}>
                      <Touchable underlayColor="#fff" onPress={onPressImage}>
                        <RobotoBoldText style={styles.recipeName}>{meal["name"].toUpperCase()}</RobotoBoldText>
                      </Touchable>
                  </View> 
                  <View style={styles.cardBodyFooterContainer}>
                    <View style={styles.flexRow}>
                      <Feather style={styles.clock} name="clock" size={15} color={Colors.default.darkGrey} />
                      <RobotoRegularText style={styles.clockText}>00h{meal.duration}</RobotoRegularText>
                    </View>
                  </View>
                </View>
                <View style={styles.cardImage}>
                    <Touchable underlayColor="#fff"  onPress={onPressImage}>
                      <Image style={styles.circleImage} source={Images.getImage(meal.image, true)} />
                    </Touchable>
                </View> 
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
      borderWidth: 2,
      borderColor: '#6cc8be',
      borderRadius: 8,
      width: '100%',
      padding: 8,
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      marginBottom: 4,
    },
    flexRow: {
      flexDirection: 'row',
      alignItems: 'center', 
    },
    circle: {
      width: 100,
      height: 100,
      borderRadius: 50,
      backgroundColor: Colors.default.whiteGrey,
      borderWidth: 3,
      borderColor: Colors.default.blueGreen,
    },
    circleImage: {
      width: 100,
      height: 100,
      borderRadius: 50,
      borderWidth: 3,
      borderColor: Colors.default.blueGreen,
    },
    cardBodyFooterContainer: {
      flexDirection: 'row',
      justifyContent: 'flex-start' 
    },
    cardImage: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
    },
    cardBodyContainer: {
      flexDirection: 'column',
      padding: 8,
      width: _deviceIsSmall() ? '55%' : '60%',   
    },
    cardBodyTitleContainer: {
      flexDirection: 'column',
      paddingBottom: 8,
      flexWrap: 'wrap'
    },
    cardBodyNameContainer: {
      paddingBottom: 8,
      flexDirection:'row',
      width: '100%', 
    },
    recipeName: {
      fontSize: 25,
      color: Colors.default.framboise,
      flexWrap: 'wrap'
    },
    mealType: {
      fontSize: 15,
      color: Colors.default.darkGrey
    },
    day: {
      fontSize: 30,
      color: Colors.default.darkGrey
    },
    clock: {
      paddingRight: 4,
    },
    clockText: {
      fontSize: 15,
      color: Colors.default.darkGrey
    },
    user: {
      paddingRight: 4,
      paddingLeft: 8,
    },
    userText: {
      fontSize: 15,
      color: Colors.default.darkGrey
    },
  });

export default MealSelectedCard;